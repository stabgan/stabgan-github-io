function interpolateColor(color1, color2, factor) {
    if (arguments.length < 3) { 
        factor = 0.5; 
    }
    var result = color1.slice();
    for (var i = 0; i < 3; i++) {
        result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
    }
    return result;
};
// My function to interpolate between two colors completely, returning an array
function interpolateColors(color1, color2, steps) {
    var stepFactor = 1 / (steps - 1),
        interpolatedColorArray = [];

    color1 = color1.match(/\d+/g).map(Number);
    color2 = color2.match(/\d+/g).map(Number);

    for(var i = 0; i < steps; i++) {
        interpolatedColorArray.push(interpolateColor(color1, color2, stepFactor * i));
    }

    return interpolatedColorArray;
}

var colorArray = interpolateColors("rgb(255, 255, 255)", "rgb(104,93,121)", 10);


$(window).on("load", function() {

	$(".loader .inner").fadeOut(500, function() {
		$(".loader").fadeOut(750);
	});

})

$(document).ready(function() {

	new GridScrollFx( document.getElementById( 'grid' ), {
				viewportFactor : 0.4
			} );


	$('#slides').superslides({
		animation : 'fade',
		play: 5000,
		pagination :false
	})

	var typed = new Typed(".typed" , {
	strings: ["Data Scientist","Web Developer","Android Developer","Blockchain Enthusiast","Creative Coder"],
	typeSpeed: 70,
	loop: true,
	startDelay: 1000,
	backSpeed: 20,
	backDelay: 1000,
	showCursor: false,

	});




	$('.owl-carousel').owlCarousel({
    	loop:true,
    	center:true,
	    items: 10,
	    margin: 15,
	    responsiveClass:true,
	    autoplay:true,
		autoplayTimeout:1500,
		autoplayclickPause:true,
	    navText: [
	      "<i class='fa fa-chevron-left'></i>",
	      "<i class='fa fa-chevron-right'></i>"
	    ],
	    nav: true,
    	responsive:{
        0:{
            items:1
        },
        480:{
            items:2
        },
        768:{
            items:3
        },
        938:{
            items:4
        }
    }
	});

	

	var skillsTopOffSet = $(".skillsSection").offset().top;
	var statsTopOffSet = $(".statsSection").offset().top;
	var countUpRunning = true;
	$(window).scroll(function(){
		if(window.pageYOffset > skillsTopOffSet - $(window).height() + 200){
			$('.chart').easyPieChart({
	            easing : "easeInOut",
	            barColor: "#fff",
	            trackColor: false,
	            scaleColor: false,
	            lineWidth: 4,
	            size: 152,
	            onStep: function(from , to ,percent){
	            	$(this.el).find('.percent').text(Math.round(percent))
	            },
	        });
		}

		if(countUpRunning && window.pageYOffset > statsTopOffSet - $(window).height() + 200){
				$(".counter").each(function() {
				var element = $(this);
				var endVal = parseInt(element.text());
				element.countup(endVal);
			});
				countUpRunning = false;
		}
	});

	


	$("#navigation li a").click(function(e) {
		// e.preventDefault();

		var targetElement = $(this).attr("href");
		var targetPosition = $(targetElement).offset().top;
		$("html, body").animate({ scrollTop: targetPosition - 50 }, "slow");

	});




	const nav = $("#navigation");
	const navTop = nav.offset().top;

	$(window).on("scroll", stickyNavigation);

	function stickyNavigation() {

		var body = $("body");

		if($(window).scrollTop() >= navTop) {
			body.css("padding-top", nav.outerHeight() + "px");
			body.addClass("fixedNav");
		}
		else {
			body.css("padding-top", 0);
			body.removeClass("fixedNav");
		}
	}
	// ------------

			if(window.innerWidth > 720) {
				$(document).mousemove(function(event){ 
			  		if (event.pageX > window.innerWidth*0.5){
			  			$(".shifting").css("left",-(event.pageX-window.innerWidth*0.5)/10+"px");
			  		}
			  		else {
			  			$(".shifting").css("left",(window.innerWidth*0.5-event.pageX)/10+"px");
			  		}
				});
			}
			else {
				$(".cell").css("border","1px solid white");
				$(".s1").css("background-color",a2r(colorArray[8]));
				$(".s2").css("background-color",a2r(colorArray[7]));
				$(".s3").css("background-color",a2r(colorArray[6]));
				$(".s4").css("background-color",a2r(colorArray[9]));
				$(".s5").css("background-color",a2r(colorArray[9]));
				$(".s6").css("background-color",a2r(colorArray[5]));
				$(".s7").css("background-color",a2r(colorArray[6]));
				$(".s8").css("background-color",a2r(colorArray[6]));
				$(".s9").css("background-color",a2r(colorArray[8]));
				$(".s10").css("background-color",a2r(colorArray[9]));
				$(".s11").css("background-color",a2r(colorArray[7]));
				$(".s12").css("background-color",a2r(colorArray[8]));
				$(".s13").css("background-color",a2r(colorArray[4]));
				$(".s14").css("background-color",a2r(colorArray[6]));
				$(".s15").css("background-color",a2r(colorArray[4]));
				$(".s16").css("background-color",a2r(colorArray[8]));
				$(".s17").css("background-color",a2r(colorArray[7]));
				$(".s18").css("background-color",a2r(colorArray[6]));
				$(".s19").css("background-color",a2r(colorArray[7]));
				$(".s20").css("background-color",a2r(colorArray[7]));
				$(".s21").css("background-color",a2r(colorArray[8]));
				$(".s22").css("background-color",a2r(colorArray[7]));
				$(".s23").css("background-color",a2r(colorArray[6]));
				$(".s24").css("background-color",a2r(colorArray[8]));
				$(".cell").removeClass("s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 s14 s15 s16 s17 s18 s19 s20 s21 s22 s23 s24");
			}

				

				function a2r(array){
					return "rgb("+array[0]+","+array[1]+","+array[2]+")"
				}

				const temp1 = $(".s1").html();
				$(".s1").css("background-color",a2r(colorArray[8]));
				$(".s1").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp1);
				                }
				         );
				
				const temp2 = $(".s2").html();
				$(".s2").css("background-color",a2r(colorArray[7]));
				$(".s2").hover(
				                function(){
				                    $(this).html("<h1>70%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp2);
				                }
				         );
				const temp3 = $(".s3").html();
				$(".s3").css("background-color",a2r(colorArray[6]));
				$(".s3").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp3);
				                }
				         );

				const temp4 = $(".s4").html();
				$(".s4").css("background-color",a2r(colorArray[9]));
				$(".s4").hover(
				                function(){
				                    $(this).html("<h1>90%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp4);
				                }
				         );
				const temp5 = $(".s5").html();
				$(".s5").css("background-color",a2r(colorArray[9]));
				$(".s5").hover(
				                function(){
				                    $(this).html("<h1>95%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp5);
				                }
				         );
				const temp6 = $(".s6").html();
				$(".s6").css("background-color",a2r(colorArray[5]));
				$(".s6").hover(
				                function(){
				                    $(this).html("<h1>50%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp6);
				                }
				         );
				const temp7 = $(".s7").html();
				$(".s7").css("background-color",a2r(colorArray[6]));
				$(".s7").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp7);
				                }
				         );
				const temp8 = $(".s8").html();
				$(".s8").css("background-color",a2r(colorArray[6]));
				$(".s8").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp8);
				                }
				         );
				const temp9 = $(".s9").html();
				$(".s9").css("background-color",a2r(colorArray[8]));
				$(".s9").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp9);
				                }
				         );
				const temp10 = $(".s10").html();
				$(".s10").css("background-color",a2r(colorArray[9]));
				$(".s10").hover(
				                function(){
				                    $(this).html("<h1>95%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp10);
				                }
				         );
				const temp11 = $(".s11").html();
				$(".s11").css("background-color",a2r(colorArray[7]));
				$(".s11").hover(
				                function(){
				                    $(this).html("<h1>65%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp11);
				                }
				         );
				const temp12 = $(".s12").html();
				$(".s12").css("background-color",a2r(colorArray[8]));
				$(".s12").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp12);
				                }
				         );
				const temp13 = $(".s13").html();
				$(".s13").css("background-color",a2r(colorArray[4]));
				$(".s13").hover(
				                function(){
				                    $(this).html("<h1>40%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp13);
				                }
				         );
				const temp14 = $(".s14").html();
				$(".s14").css("background-color",a2r(colorArray[6]));
				$(".s14").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp14);
				                }
				         );
				const temp15 = $(".s15").html();
				$(".s15").css("background-color",a2r(colorArray[4]));
				$(".s15").hover(
				                function(){
				                    $(this).html("<h1>40%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp15);
				                }
				         );
				const temp16 = $(".s16").html();
				$(".s16").css("background-color",a2r(colorArray[8]));
				$(".s16").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp16);
				                }
				         );
				const temp17 = $(".s17").html();
				$(".s17").css("background-color",a2r(colorArray[7]));
				$(".s17").hover(
				                function(){
				                    $(this).html("<h1>70%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp17);
				                }
				         );
				const temp18 = $(".s18").html();
				$(".s18").css("background-color",a2r(colorArray[6]));
				$(".s18").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp18);
				                }
				         );
				const temp19 = $(".s19").html();
				$(".s19").css("background-color",a2r(colorArray[7]));
				$(".s19").hover(
				                function(){
				                    $(this).html("<h1>70%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp19);
				                }
				         );
				const temp20 = $(".s20").html();
				$(".s20").css("background-color",a2r(colorArray[7]));
				$(".s20").hover(
				                function(){
				                    $(this).html("<h1>70%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp20);
				                }
				         );
				const temp21 = $(".s21").html();
				$(".s21").css("background-color",a2r(colorArray[8]));
				$(".s21").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp21);
				                }
				         );
				const temp22 = $(".s22").html();
				$(".s22").css("background-color",a2r(colorArray[7]));
				$(".s22").hover(
				                function(){
				                    $(this).html("<h1>70%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp22);
				                }
				         );
				const temp23 = $(".s23").html();
				$(".s23").css("background-color",a2r(colorArray[6]));
				$(".s23").hover(
				                function(){
				                    $(this).html("<h1>60%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp23);
				                }
				         );
				const temp24 = $(".s24").html();
				$(".s24").css("background-color",a2r(colorArray[8]));
				$(".s24").hover(
				                function(){
				                    $(this).html("<h1>80%</h1>");
				                }, 
				                function(){
				                    $(this).html(temp24);
				                }
				         );

	
		
		
				


});

console.log("running")